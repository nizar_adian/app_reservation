<?php 
namespace App\Controllers;
use App\Models\M_Login;

class Login extends BaseController
{

	public function __construct(){
		helper(['form']);
	}

	public function index()
	{
		return view('v_login');
	}

	public function verification()
    {
		$session = session();
		$model = new M_Login();
		$username = $this->request->getVar('username');
		$password = $this->request->getVar('password');
		
		$model->where('username', $username);
		$model->where('isactive', 1);
        $data = $model->first();
        if($data){
            $dtpass = $data['password'];
            $verify_pass = password_verify($password, $dtpass);
            if($verify_pass){
                $ses_data = [
                    'user_id'       => $data['user_id'],
                    'username'      => $data['username'],
					'email'   		=> $data['email'],
					'firstname'   	=> $data['firstname'],
					'lastname'   	=> $data['lastname'],
					'client_id'   	=> $data['client_id'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/home');
            }else{
                $session->setFlashdata('msg', 'Wrong Password');
                return redirect()->to('/login');
            }
        }else{
            $session->setFlashdata('msg', 'Username not Found');
            return redirect()->to('/login');
        }

	}

	//--------------------------------------------------------------------

}

<?php
namespace App\Models;
 
use CodeIgniter\Model;
 
class M_Login extends Model{
    protected $table = 'm_user';
    protected $allowedFields = ['user_id','username','password','email','isactive','firstname','lastname'];
}
<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RoleMenuSeeder extends Seeder
{
	public function run()
	{
		// membuat data		
		$role[1] = array(1,2,3,4,5,6);
		$role[2] = array(2,3,4,5,6);
		$role[3] = array(3);

		$n = 3;
		for ($i=1; $i <=3 ; $i++) { 
			$data['client_id'] = 101;
			$data['role_id'] = $i;
			$data['create_by'] = 'superuser';

			foreach($role[$i] as $menu){
				// insert semua data ke tabel
				$data['menu_id'] = $menu;
				$this->db->table('m_role_menu')->insert($data);
			}

		}
		
	}
}

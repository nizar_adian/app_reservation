<?php 
namespace App\Database\Seeds;
use CodeIgniter\Database\Seeder;

class DatabaseSeeder extends Seeder
{
        public function run()
        {
                $this->call('ClientSeeder');                
                $this->call('MenuSeeder');
                $this->call('RoleSeeder');
                $this->call('AdminSeeder'); //user Superadmin
                $this->call('RoleMenuSeeder');
                $this->call('UserRoleSeeder');
        }
}
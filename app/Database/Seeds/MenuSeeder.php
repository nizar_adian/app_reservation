<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MenuSeeder extends Seeder
{
	public function run()
	{
		// membuat data
		$batch_data = [
			[
				'client_id' => 101,
				'menu_id'  => 1,
				'nama_menu'  => 'Office',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,				
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'menu_id'  => 2,
				'nama_menu'  => 'Product',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,				
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'menu_id'  => 3,
				'nama_menu'  => 'Reservation',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,				
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'nama_menu'  => 'Company Profile',
				'path'  => '/company',
				'icon'  => 'mdi mdi-sale menu-icon',	
				'parent_id' => 1,					
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'nama_menu'  => 'Employee',
				'path'  => '/employee',
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => 1,						
				'create_by' => 'superuser'
			],	
			[
				'client_id' => 101,
				'nama_menu'  => 'Tax & Currency',
				'path'  => '/taxcurrency',
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => 1,				
				'create_by' => 'superuser'
			],			
		];

		foreach($batch_data as $data){
			// insert semua data ke tabel
			$this->db->table('m_menu')->insert($data);
		}
	}
}

<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RoleSeeder extends Seeder
{
	public function run()
	{
		// membuat data
		$batch_data = [
			[
				'client_id' => 101,
				'role_id'  => 1,
				'nama_role'  => 'SuperUser',		
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'role_id'  => 2,
				'nama_role'  => 'Admin',		
				'create_by' => 'superuser'
			],
			[
				'client_id' => 101,
				'role_id'  => 3,
				'nama_role'  => 'Cashier',		
				'create_by' => 'superuser'
			],
		];

		foreach($batch_data as $data){
			// insert semua data ke tabel
			$this->db->table('m_role')->insert($data);
		}
	}
}

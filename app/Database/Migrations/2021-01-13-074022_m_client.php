<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MClient extends Migration
{
	public function up()
	{
		//field dari table m_user
		$fields = [
			'client_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'nama_client'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'token'      => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'unique'         => TRUE
			],
			'premium' => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'     	=> 0,
			],			
			'created_at datetime default current_timestamp',		
			'create_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],			
			'updated_at datetime default current_timestamp on update current_timestamp',
			'updated_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],
		];

		//create database
		$this->forge->addField($fields);

		// Membuat primary key
		$this->forge->addKey('client_id', TRUE);

		// Membuat tabel news
		$this->forge->createTable('m_client', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('m_client');
	}
}

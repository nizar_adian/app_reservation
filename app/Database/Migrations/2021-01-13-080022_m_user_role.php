<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MUserRole extends Migration
{
	public function up()
	{
		//field dari table m_user
		$fields = [
			'client_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'user_role_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'user_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
			],
			'role_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
			],			
			'created_at datetime default current_timestamp',		
			'create_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],			
			'updated_at datetime default current_timestamp on update current_timestamp',
			'updated_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],		
		];
		
		//create field
		$this->forge->addField($fields);

		// Membuat primary key
		$this->forge->addKey('user_role_id', TRUE);
		//iki foreign id
		// $this->forge->addForeignKey('user_id','m_user','user_id');
		// $this->forge->addForeignKey('role_id','m_role','role_id');
		$this->forge->addForeignKey('client_id','m_client','client_id');

		// Membuat tabel news
		$this->forge->createTable('m_user_role', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('m_user_role');
	}
}

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Enhance Purple Admin</title>
    
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?=base_url();?>/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?=base_url();?>/assets/vendors/css/vendor.bundle.base.css">
    <script type="text/javascript" src="<?=base_url('Plugin/js/jquery-3.3.1.min.js');?>"></script>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?=base_url();?>/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?=base_url();?>/assets/images/favicon.ico" />

       
    <!-- Plugin CSS DataTables -->    
    
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/Plugin/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/Plugin/css/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/Plugin/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/Plugin/css/responsive.bootstrap4.min.css"/>

  </head>
  <body>

    <?= $this->include('layout/header') ?>
    <?= $this->include('layout/navbar') ?>
      <!-- partial -->
      <div class="main-panel">
          <div class="content-wrapper">    
            <?= $this->renderSection('content') ?>
          </div>
        <!-- page-body-wrapper ends -->
      </div>
      <!-- container-scroller -->
    
    <?= $this->include('layout/footer') ?>
         

    
    <!-- plugins:js -->
    <script src="<?=base_url();?>/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="<?=base_url();?>/assets/vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?=base_url();?>/assets/js/off-canvas.js"></script> 
    <script src="<?=base_url();?>/assets/js/hoverable-collapse.js"></script>
    <script src="<?=base_url();?>/assets/js/misc.js"></script> 
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="<?=base_url();?>/assets/js/dashboard.js"></script>
    <script src="<?=base_url();?>/assets/js/todolist.js"></script>
    <!-- End custom js for this page -->

    
    <script type="text/javascript" src="<?=base_url('Plugin/js/jquery.dataTables.min.js');?>"></script>    
    <script type="text/javascript" src="<?=base_url('Plugin/js/dataTables.bootstrap4.min.js');?>"></script> 
    <script type="text/javascript" src="<?=base_url('Plugin/js/dataTables.responsive.min.js');?>"></script> 
    <script type="text/javascript" src="<?=base_url('Plugin/js/responsive.bootstrap4.min.js');?>"></script> 
    
  </body>
</html>